using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        public int Id_Vendedor { get; set; }
        public string CpfVendedor { get; set; }
        public String NomeVendedor { get; set; }
        public String EmailVendedor { get; set; }
        public String TelefoneVendedor { get; set; }
    }
}