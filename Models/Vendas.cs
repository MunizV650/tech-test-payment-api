using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Vendas
    {
        public int Id_Vendas { get; set; }
        public int Id_Vendedor { get; set; }
        public int Numero_Pedido { get; set; }
        public String Itens_Vendidos { get; set; }

        public DateTime Data { get; set; }
        public EnumStatusVendas Status { get; set; }
    }
}