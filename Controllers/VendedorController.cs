using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        [HttpPost("Adicionar Vendedor")]
        public IActionResult Criar(Vendedor vendedor)
        {
      return Ok(vendedor);
    }
    }
}